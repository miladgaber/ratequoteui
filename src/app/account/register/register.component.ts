import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/Services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public signupForm !: FormGroup;
  constructor(private formBuilder: FormBuilder , private userService:UserService) { }

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      username:['',Validators.required],
      email:['',Validators.required],
      password:['',Validators.required],
      usertype:['',Validators.required]
    })
  }

  onSubmit(){
    console.log("on submit register")
    let username = this.signupForm.controls["username"].value; 
    let email = this.signupForm.controls["email"].value; 
    let password = this.signupForm.controls["password"].value; 
    this.userService.register(username,email,password).subscribe((data)=>{
      console.log("response",data)
    },error=>{
      console.error(error);
    })
  }
}
