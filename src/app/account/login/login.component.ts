import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/Services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm !: FormGroup;
  constructor(private formBuilder: FormBuilder , private userService:UserService ,private router:Router) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email:['',Validators.required],
      password:['',Validators.required]
    })
  }
 
  errorMessage:any ;

  onSubmit(){
    console.log("on submit register")
    let email = this.loginForm.controls["email"].value; 
    let password = this.loginForm.controls["password"].value; 
    this.userService.login(email,password).subscribe((data)=>{
      if(data.isSuccess){
        localStorage.setItem("userInfo",JSON.stringify(data.message));
        this.router.navigate(['/rate-quote'])
      }
      console.log("response",data)
    },error=>{
      console.error(error);
    })
  }

}
