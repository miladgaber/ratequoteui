import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountComponent } from './account/account.component';
import { ForgetPasswordComponent } from './account/forget-password/forget-password.component';
import { LoginComponent } from './account/login/login.component';
import { RegisterComponent } from './account/register/register.component';
import { ResetPasswordComponent } from './account/reset-password/reset-password.component';
import { RateQuoteFormComponent } from './rate-quote/rate-quote-form/rate-quote-form.component';
import { RateQuoteComponent } from './rate-quote/rate-quote.component';

const routes: Routes = [
    {path:'',redirectTo:'account/login' ,pathMatch:'full'},
    {path:'account',component:AccountComponent},
    {path:'account/login',component:LoginComponent},
    {path:'account/register',component:RegisterComponent},
    {path:'account/forget-password',component:ForgetPasswordComponent},
    {path:'account/reset-password',component:ResetPasswordComponent},
    {path:'rate-quote',component:RateQuoteComponent},
    {path:'rate-quote/rate-quote-form',component:RateQuoteFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
