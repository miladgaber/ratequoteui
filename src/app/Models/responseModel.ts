//import { ResponseCode } from "../enums/responseCode";

/**
 * ResponseModel
 */

export class ResponseModel{
  public  isSuccess :boolean = false;
  public message:string ="";
  public  errors:string[]  =[];
  public  exp:string ="";

}