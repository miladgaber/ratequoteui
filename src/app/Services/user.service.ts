import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ResponseModel } from '../Models/responseModel';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly baseURL:string="https://localhost:44386/api/Authenticate/"
  constructor(private httpClient:HttpClient , private router :Router) { }

  public login(email:string , password:string)
   {
     const body={
       Email:email,
       Password:password
     }
    return this.httpClient.post<ResponseModel>(this.baseURL+"login",body);
   }

   public register(username:string ,email:string , password:string)
   {
     const body={
       Username:username,
       Email:email,
       Password:password
     }
    //  this.router.navigate(['account/login']);
    return this.httpClient.post(this.baseURL+"register",body);
   }

   public forget(email:string)
   {
     const body={
       Email:email,
     }
    return this.httpClient.post(this.baseURL+"forget-password",email);
   }
   public reset(token:string ,email:string , newPassword:string,confirmPassword:string)
   {
     const body={
       Token:token,
       Email:email,
       NewPassword:newPassword,
       ConfirmPassword:confirmPassword
     }
    return this.httpClient.post(this.baseURL+"reset-password",body);
   }

}
