import { TestBed } from '@angular/core/testing';

import { RateQuoteService } from './rate-quote.service';

describe('RateQuoteService', () => {
  let service: RateQuoteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RateQuoteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
