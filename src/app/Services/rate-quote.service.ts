
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Pallet } from '../Models/pallet';
import { RateQuoteResponseModel } from '../Models/rateQuoteResponseModel';

@Injectable({
  providedIn: 'root'
})
export class RateQuoteService {

  private readonly baseURL: string = "https://localhost:44386/api/RateQuote/"
  constructor(private httpClient: HttpClient, private router: Router) { }

  public palletTypes() {
    let userInfo = JSON.parse(JSON.stringify(localStorage.getItem("userInfo")));
    console.log("userInfo : " + userInfo);
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${userInfo}`
    });

    return this.httpClient.get<RateQuoteResponseModel>(this.baseURL + "PalletTypes", { headers: headers }).pipe(map(res => {
      let userList = new Array<Pallet>();
      if (res.wasSuccess) {
        if (res.result) {
          res.result.map((x: Pallet) => {
            userList.push(new Pallet(x.code, x.description));
          })
        }
      }
      return userList;
    }));
  }


}
