import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RateQuoteFormComponent } from './rate-quote-form.component';

describe('RateQuoteFormComponent', () => {
  let component: RateQuoteFormComponent;
  let fixture: ComponentFixture<RateQuoteFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RateQuoteFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RateQuoteFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
