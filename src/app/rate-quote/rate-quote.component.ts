import { Component, OnInit } from '@angular/core';
import { RateQuoteService } from '../Services/rate-quote.service';
import { Pallet } from '../Models/pallet';


@Component({
  selector: 'app-rate-quote',
  templateUrl: './rate-quote.component.html',
  styleUrls: ['./rate-quote.component.css']
})
export class RateQuoteComponent implements OnInit {

  constructor(private rateQuoteService: RateQuoteService) { }
  public palletList: Pallet[] = [];
  ngOnInit(): void {
    this.getAllPalletTypes();
  }

  getAllPalletTypes(){
    
    this.rateQuoteService.palletTypes().subscribe((data:Pallet[]) =>{
      this.palletList = data;
    })
    console.log("getAllPalletTypes called")
  }


}
